package edu.bkap.savefile;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class FileManagerActivity extends AppCompatActivity {
    final String TAG = "FileLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);

        String fileName = "bkap-text.txt";
        String content = "Bach khoa aptech course";
//        writeLocal(fileName, content);
//        this.readLocal(fileName);

        listDirectory();
    }


    private void writeLocal(String fileName, String content){
        String arrFile[] = this.fileList();
        if(arrFile.length == 0){
            Log.i(TAG, "No current files storage internally. Creating one\n");
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput(fileName, Context.MODE_PRIVATE));
                BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                bufferedWriter.write(content);
                bufferedWriter.newLine();
                bufferedWriter.flush();
                bufferedWriter.close();
                outputStreamWriter.close();
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }

        if(arrFile.length > 0){
            for (int i = 0; i < arrFile.length; ++i){
                Log.i(TAG, arrFile[i] + "\n");
                Log.i(TAG, "Now append to it \n");
                try {
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput(arrFile[i], Context.MODE_APPEND));
                    BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
                    bufferedWriter.write(content);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                    bufferedWriter.close();
                    outputStreamWriter.close();

                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void readLocal(String fileName){
        try {
            InputStreamReader inputStreamReader = new InputStreamReader(this.openFileInput(fileName));
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = bufferedReader.readLine();
            while (line != null){
                Log.i(TAG, line + "\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            inputStreamReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeExternal(String fileName, String content){
        FileOutputStream fos = null;

        try {
            final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/folderName/" );

            if (!dir.exists())
            {
                if(!dir.mkdirs()){
                    Log.e("ALERT","could not create the directories");
                }
            }

            final File myFile = new File(dir, fileName + ".txt");

            if (!myFile.exists())
            {
                myFile.createNewFile();
            }

            fos = new FileOutputStream(myFile);

            fos.write(content.getBytes());
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private String readExternal(String fileName){
        //Find the directory for the SD Card using the API
//*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();

//Get the text file
        File file = new File(sdcard,fileName);

//Read text from file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        }
        catch (IOException e) {
            //You'll need to add proper error handling here
            e.printStackTrace();
        }

//Find the view by its id
//        TextView tv = (TextView)findViewById(R.id.text_view);
//
////Set the text
//        tv.setText(text);
        return text.toString();
    }

    private void listDirectory(){
        File[] externalStorageVolumes =
                ContextCompat.getExternalFilesDirs(getApplicationContext(), null);
//        File primaryExternalStorage = externalStorageVolumes[0];
        for (File file : externalStorageVolumes) {
            Log.i(TAG, file.getAbsolutePath());
        }
    }

    private boolean isExternalStorageWritable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    // Checks if a volume containing external storage is available to at least read.
    private boolean isExternalStorageReadable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED) ||
                Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @org.jetbrains.annotations.NotNull String[] permissions, @NonNull @org.jetbrains.annotations.NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i(TAG, permissions.toString());
    }
}